package com.openclassrooms.medilabo.auth.UT.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.openclassrooms.medilabo.auth.config.JwtConfig;
import com.openclassrooms.medilabo.auth.service.JwtTokenService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.crypto.spec.SecretKeySpec;
import java.util.Date;

@ExtendWith(MockitoExtension.class)
public class JwtTokenServiceTest {

    @Mock
    private JwtConfig jwtConfig;

    @InjectMocks
    private JwtTokenService jwtTokenService;

    @Test
    public void testCreateTokenReturnsValidToken() {
        //Given
        when(jwtConfig.getSecretKey()).thenReturn("secretKey123456789012345678901234567890");
        Authentication authentication = new UsernamePasswordAuthenticationToken("user", "password", AuthorityUtils.createAuthorityList("ROLE_USER"));
        //When
        String token = jwtTokenService.createToken(authentication);
        // Then
        assertThat(token).isNotBlank();
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(new SecretKeySpec(jwtConfig.getSecretKey().getBytes(), "HmacSHA512"))
                .build()
                .parseClaimsJws(token)
                .getBody();
        Date now = new Date();
        Date expectedExpiration = new Date(now.getTime() + 3600000);
        long difference = claims.getExpiration().getTime() - expectedExpiration.getTime();
        assertThat(Math.abs(difference)).isLessThan(10000);
        assertThat(claims.getSubject()).isEqualTo("user");
        assertThat(new Date().before(claims.getExpiration())).isTrue();
    }
}
