package com.openclassrooms.medilabo.auth.dto;


public record JwtAuthenticationResponse(String bearer) {
}
