package com.openclassrooms.medilabo.auth.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(name = "LoginRequest")
public record LoginRequest(
        @NotBlank
        String username,
        @NotBlank
        String password
) {
}
