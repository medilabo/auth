package com.openclassrooms.medilabo.auth.controller;

import com.openclassrooms.medilabo.auth.dto.JwtAuthenticationResponse;
import com.openclassrooms.medilabo.auth.service.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import com.openclassrooms.medilabo.auth.dto.LoginRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
@Tag(name = "Auth")
public class AuthController {

    private final AuthService authService;

    @PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
    @Operation(operationId = "authentication")
    public ResponseEntity<JwtAuthenticationResponse> authentication(@RequestBody LoginRequest loginRequest) {
        String jwt = authService.authenticateAndGenerateToken(
                loginRequest.username(), loginRequest.password());
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }
    
}
